@echo off

net stop Audiosrv
net stop AudioEndpointBuilder

reg delete /f "HKCU\Software\Microsoft\Internet Explorer\LowRegistry\Audio\PolicyConfig\PropertyStore"
reg add "HKCU\Software\Microsoft\Internet Explorer\LowRegistry\Audio\PolicyConfig\PropertyStore"

net start Audiosrv

pause
