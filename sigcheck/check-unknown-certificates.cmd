@echo off
cd /d %~dp0
md output >NUL 2>&1
sigcheck.exe -accepteula -tv -w output\unknown-machine-certificates.txt 2>NUL
sigcheck.exe -accepteula -tuv -w output\unknown-user-certificates.txt 2>NUL
pause
