@echo off
cd /d %~dp0
md output >NUL 2>&1
sigcheck.exe -accepteula -e -s -c -w output\signatures.csv %SystemRoot%\System32 2>NUL
pause
